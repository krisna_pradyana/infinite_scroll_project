﻿// C# example.
using UnityEditor;
using System.Diagnostics;

public class ScriptBatch
{
    [MenuItem("MyTools/Android Build")]
    public static void BuildGame()
    {
        // Get filename.
        //string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
        string[] levels = new string[] { "Assets/Scene/RecyclingList.unity" };
    
        // Build player.
        BuildPipeline.BuildPlayer(levels, "C:/BuiltGame.exe", BuildTarget.StandaloneWindows, BuildOptions.Development);
    
        // Copy a file from the project folder to the build folder, alongside the built game.
        FileUtil.CopyFileOrDirectory("Assets/Templates/Readme.txt", "C:/BuildReadme.txt");
    
        // Run the game (Process class from System.Diagnostics).
        Process proc = new Process();
        proc.StartInfo.FileName = "C:/BuiltGame.exe";
        proc.Start();
    }
}

