﻿using UnityEngine;
using UnityEngine.UI;

public class DebugBox : MonoBehaviour
{
    [SerializeField] Text relativeText;
    [SerializeField] Text distanceText;

    public Vector2 RectTrans
    {
        set
        {
            GetComponent<Transform>().position = value;
        }
    }

    public string Relative 
    { 
        get
        {
            return relativeText.text;
        }
        set
        {
            relativeText.text = value;
        }
    }

    public string Distance
    {
        get
        {
            return distanceText.text;
        }
        set
        {
            distanceText.text = value;
        }
    }
}
