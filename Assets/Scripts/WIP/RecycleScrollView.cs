﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RecycleScrollView : MonoBehaviour
{
    enum ScrollDirection
    {
        stationary,
        up,
        down
    }

    [Header("Main Properties")]
    [SerializeField] GameObject prefabItem;
    [SerializeField] RecycleScrollRect infiniteScrollRect;
    [SerializeField] RectTransform content;
    [SerializeField] RectTransform viewport;
    [SerializeField] int renderedItemsNum;
    [SerializeField] int spacing;

    [Header("Debug")]
    [SerializeField] bool debugGizmos; 
    [SerializeField] DebugBox debugBox;
    [SerializeField] Vector3 positionVportY;
    float previousItemHeight = 0;

    #region Private fields
    int highestRowRendered;
    int recycleCount;
    int firstRow;
    int lastRow;
    float lastScrollValue;
    Vector3 viewportPosition;
    ScrollDirection direction;
    #endregion

    #region Hidden public
    public List<TestChildData> data;
    public List<RectTransform> items;
    #endregion

    int middleRow
    {
        get 
        {
            int result = 0;
            if (items.Count%2 == 1)
            {
                result = items.Count / 2;
                
            }
            else if (items.Count%2 == 0)
            {
                result = items.Count / 2 - 1;
            }
            
            return result;
        }
    }

    float totalItemsHeight
    {
        get
        { 
            return items.Sum((x) => x.rect.height);
        }
    }

    float totalSpacing
    {
        get
        {
            return spacing * items.Count;
        }
    }

    Vector3 PositionVPortY
    {
        set
        {
            positionVportY = value;
        }
    }

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        infiniteScrollRect.onValueChanged.AddListener(GetScrollDir);
    }

    void GetScrollDir(Vector2 value)
    {
        Vector2 result = value;
        //if (Mathf.Abs(result.y) - Mathf.Abs(lastScrollValue) > lastScrollValue)
        //{
        //    direction = ScrollDirection.up;
        //}
        Debug.Log(result);
    }

    public IEnumerator CreateItems()
    {
        //1.spawn item one by one based on their height
        //2.limit visible item based on number 

        yield return new WaitUntil(() => data.TrueForAll((value) => value.height > 0));

        if (renderedItemsNum > 3)
        {
            for (int i = 0; i < renderedItemsNum; i++)
            {
                int index = i;
                var obj = Instantiate(prefabItem, content.transform);
                obj.name = i.ToString();
                var rectTrans = obj.GetComponent<RectTransform>();
                var itemData = rectTrans.GetComponent<TestChildItem>();
            
                itemData.ChildData = data[index];
                rectTrans.anchoredPosition = new Vector2(rectTrans.anchoredPosition.x, rectTrans.anchoredPosition.y + previousItemHeight);
                
                previousItemHeight -= rectTrans.rect.height + spacing;
                rectTrans.FitHorizontally();
                items.Add(rectTrans);
            }
            
            content.SetHeight(data.Sum((x) => x.height + spacing) - spacing);
            firstRow = 0;        
            lastRow = items.Count - 1;
            RefreshContent();
        }
        else
        {

        }
    }

    private void Update()
    {
        OnScroll();

        if (items.Count > 3)
        {
            if (debugGizmos)
            {
                Debug.DrawLine(new Vector2(viewport.position.x, viewport.position.y /2), new Vector2(items[middleRow].position.x, items[middleRow].position.y ));            
                //Debug.Log(items[middleRow].anchoredPosition.y);
                debugBox.Relative = "Relative : " + items[middleRow].gameObject.name;
            }
        }
        
        ViewportPosition();
    }

    void OnScroll()
    {
        Debug.Log("total height : " + totalItemsHeight);

        //get sibling index
        if (items.Count < data.Count && items.Count > 0)
        {
            if (viewportPosition.y / 2 > items[middleRow].rect.height / 2 + spacing) // based height last items
            {
                if (recycleCount + items.Count > data.Count - 1)
                {
                    return;
                }
                Vector2 tempVelocity = infiniteScrollRect.velocity;
                RectTransform temp = items[firstRow];

                recycleCount += 1;
                temp.GetComponent<TestChildItem>().ChildData = data[(items.Count-1) + recycleCount];
                highestRowRendered = 1 + items.Count + recycleCount;

                items[firstRow].SetAsLastSibling();
                items[firstRow].anchoredPosition = new Vector2(items[firstRow].position.x, items[lastRow].anchoredPosition.y - items[lastRow].rect.height - spacing);
                items[firstRow].FitHorizontally();

                items.Remove(items[firstRow]);
                items.Add(temp);

                infiniteScrollRect.velocity = tempVelocity;
                RefreshContent();
            }

           if (viewportPosition.y / 2 < items[middleRow].rect.height / 2 + spacing) // based height last items
           {
               if (highestRowRendered - (highestRowRendered - recycleCount) <= 0)
               {
                   return;
               }
               Vector2 tempVelocity = infiniteScrollRect.velocity;
               RectTransform temp = items[lastRow];
           
               recycleCount -= 1;
               temp.GetComponent<TestChildItem>().ChildData = data[highestRowRendered - (highestRowRendered - recycleCount)];
               highestRowRendered -= 1;
           
               items[lastRow].SetAsFirstSibling();
               items[lastRow].anchoredPosition = new Vector2(items[lastRow].position.x, items[firstRow].anchoredPosition.y + temp.rect.height + spacing);
               items[lastRow].FitHorizontally();
               
               items.Remove(items[lastRow]);
               items.Insert(0,temp);
               
               infiniteScrollRect.velocity = tempVelocity;
               RefreshContent();
           }
        }
    }

    void ViewportPosition()
    {
        if (items.Count < 3)
        {
            return;
        }
        positionVportY = viewportPosition = (viewport.InverseTransformVector(items[middleRow].position + new Vector3(0, spacing,0)));
        debugBox.Distance = "Distance : " + Vector3.Distance(viewport.position, items[middleRow].position);
        //Debug.Log("Viewport Position relative to Content : " + viewportPosition);
    }

    /// <summary>
    /// Fitteing content to left or right of the viewport
    /// </summary>
    void RefreshContent()
    {
        content.SetRight(0);
        content.SetLeft(0);
    }
}

public static class RectTransformExtensions
{
    public static void SetLeft(this RectTransform rt, float left)
    {
        rt.offsetMin = new Vector2(left, rt.offsetMin.y);
    }

    public static void SetRight(this RectTransform rt, float right)
    {
        rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
    }

    public static void SetTop(this RectTransform rt, float top)
    {
        rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
    }

    public static void SetBottom(this RectTransform rt, float bottom)
    {
        rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
    }

    public static void SetHeight(this RectTransform rt, float height)
    {
        rt.sizeDelta = new Vector2(rt.rect.width, height);
    }

    public static void SetWidth(this RectTransform rt, float width)
    {
        rt.sizeDelta = new Vector2(width, rt.rect.height);
    }

    public static void FitHorizontally(this RectTransform rt)
    {
        rt.SetLeft(0);
        rt.SetRight(0);
    }
}
