using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[System.Serializable]
public class TestChildData {
    public string Title;
    public string Note;
    public int Row;
    public float height;
    public Color rowColor;

    public TestChildData(string t, string n, int r, float h, Color c) {
        Title = t;
        Note = n;
        Row = r;
        height = h;
        rowColor = c;
    }
}

public class TestPanel : MonoBehaviour {

    public RecycleScrollView listView;
    public List<TestChildData> data = new List<TestChildData>();

    private void Start() {

        RetrieveData();
        StartCoroutine(listView.CreateItems());

    }

    private void RetrieveData() {
        data.Clear();
        int row = 0;

        List<DummyData> dummies = new List<DummyData>();

        dummies.Add(new DummyData("Hello World", 0));
        dummies.Add(new DummyData("This is fine", 0));
        dummies.Add(new DummyData("You look nice today", 0));
        dummies.Add(new DummyData("Recycling is good", 0));
        dummies.Add(new DummyData("Why not", 0));

        for (int i = 0; i < 20; ++i) {

            Color tempColor;
            if (i%2 == 0)
            {
                tempColor = new Color(255,255,255, 40);
            }
            else
            {
                tempColor = new Color(255, 0, 0, 40);
            }

            data.Add(new TestChildData
                    (
                    dummies[Random.Range(0, dummies.Count)].message, 
                    $"Row {row++}", 
                    row, 
                    dummies[Random.Range(0, dummies.Count)].itemHeight,
                    tempColor
                    )
            );
        }

        listView.data = data;
        StartCoroutine(UpdateDataAfterFewTimes());
    }

    IEnumerator UpdateDataAfterFewTimes()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("Height is updated on realtime");
        for (int i = 0; i < data.Count; i++)
        {
            data[i].height = Random.Range(790, 1920);
        }
        listView.data = data;
    }
}

public class DummyData
{
    public string message;
    public float itemHeight;

    public DummyData (string m1, float f1)
    {
        message = m1;
        itemHeight = f1;
    }
}
