using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TestChildItem : MonoBehaviour {
    public Text leftText;
    public Text rightText1;
    public Text rightText2;
    public Image itemBg;
    
    RectTransform rectTrans { get { return GetComponent<RectTransform>(); } }

    private TestChildData childData;
    public TestChildData ChildData {
        get { return childData; }
        set {
            childData = value;
            leftText.text = childData.Title;
            rightText1.text = childData.Row.ToString();
            rightText2.text = "H : " + childData.height;
            rectTrans.SetHeight(childData.height);
            itemBg.color = childData.rowColor;
        }
    }

    private IEnumerator Start()
    {
        Debug.Log(childData.height);
        yield return null;
        RefreshItem();
    }

    public IEnumerator RefreshItem()
    {
        yield return null;
        rectTrans.SetLeft(0);
        rectTrans.SetRight(0);
    }

}
